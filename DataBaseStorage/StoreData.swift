//
//  StoreData.swift
//  DataBaseStorage
//
//  Created by surabhi on 23/02/19.
//  Copyright © 2019 organization. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import CryptoSwift

class StoreData{
    
    static let shared = StoreData()
    private init(){}
    
    let path = NSHomeDirectory()+"/Documents/Data.plist"
    var dictionary = NSMutableDictionary()
    let fileManager = FileManager.default
    let key = "ccC2H19lDDbQDfakxcrtNMQdd0FloLGG"
    let iv = "ggGGHUiDD0Qjhuvv"
    
    func getobject(Key : String) -> Any {
     
        checkFile()
        
         dictionary = NSMutableDictionary(contentsOfFile: path)!
        
        do{
             if dictionary[Key] != nil {
            let data = dictionary.object(forKey: Key) as! Data
            let decodedData = try data.aesDecrypt(key: key, iv: iv)
            dictionary = NSKeyedUnarchiver.unarchiveObject(with: decodedData) as! NSMutableDictionary
            
            if dictionary[Key] != nil {
                return dictionary.object(forKey:Key) as Any
            }
            else {
                return "No Value"
            }
            }
            
        }catch{
            return "No Value"
        }
        
        return "No Value"
    }
    
    func setObject(Value : Any , Key : String){
     
        checkFile()
        
        do{
            let dictionaryvalue = NSMutableDictionary()
            dictionaryvalue.setValue(Value, forKey: Key)
            
            let data: Data = NSKeyedArchiver.archivedData(withRootObject: dictionaryvalue)
            
            let encodedData = try data.aesEncrypt(key: key, iv: iv)
            dictionary = NSMutableDictionary(contentsOfFile: path)!
            dictionary.setValue(encodedData, forKey: Key)
           // print(dictionary)
            dictionary.write(toFile: path, atomically: true)
            
        }catch{
            
            print("error")
           
        }
    
        
    }
    
    func checkFile() {
       
        if !fileManager.fileExists(atPath: path) {
            print("File not exist!")
            
            let srcPath = Bundle.main.path(forResource: "Data", ofType: "plist")
            
            do {
                try fileManager.copyItem(atPath: srcPath!, toPath: path)
            } catch {
                print("File copy error!")
            }
        }
    }

}

extension Data {
    func aesEncrypt(key: String, iv: String) throws -> Data{
        let encypted = try AES(key: key.bytes, blockMode: CBC(iv: iv.bytes), padding: .pkcs7).encrypt(self.bytes)
        return Data(bytes: encypted)
    }
    
    func aesDecrypt(key: String, iv: String) throws -> Data{
        let decrypted = try AES(key: key.bytes, blockMode: CBC(iv: iv.bytes), padding: .pkcs7).decrypt(self.bytes)
        return Data(bytes: decrypted)
    }
}
